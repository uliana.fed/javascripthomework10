const tabs = document.querySelectorAll('.tabs-title');
const contents = document.querySelectorAll('.tabs-content li');

function switchTabLi(event) {
  tabs.forEach(tab => tab.classList.remove('active'));
  contents.forEach(content => content.style.display = 'none');
  this.classList.add('active');
  const index = Array.from(tabs).indexOf(this);
  contents[index].style.display = 'block';
}

tabs.forEach(tab => tab.addEventListener('click', switchTabLi));
